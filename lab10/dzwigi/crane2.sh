#!/bin/bash



while true; do
    sleep 3
    to_move=""
    to_move=$(ls buffer | head -n 1)
    if ! ps -p "$1" &> /dev/null && [[ -z "$to_move" ]] 
    then
        echo "Crane 2 finishing job"
        exit 0
    fi
    if [[ -z "$to_move" ]]
    then
        echo "buffer empty"
        continue
    fi
    if [[ $(ls buffer | wc -l) -gt 0 ]]
    then
        echo "taking from buffer"
        mv "buffer/$to_move" placeB/
    else
        echo "buffer empty"
    fi
done
