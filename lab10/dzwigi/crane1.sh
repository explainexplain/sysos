#!/bin/bash

counter=0

while true; do
    sleep 1
    to_move=""
    to_move=$(ls placeA | head -n 1)
    if [[ -z "$to_move" ]]
    then
        echo "Moved $counter materials"
        echo "Finishing for the day"
        exit 0
    fi
    if [[ $(ls buffer | wc -l) -le 2 ]]
    then
        ((counter++))
        echo "moving to buffer"
        mv "placeA/$to_move" buffer/
    else
        echo "Buffer full"
        
    fi
done