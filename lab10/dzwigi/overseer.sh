#!/bin/bash

echo $$
function start_cranes(){
    ./crane1.sh &
    echo $!
    ./crane2.sh $! &
    echo $!
}

function kill_cranes(){
    echo "Shutting down the cranes"
    ps -o "%p %a" | grep crane1.sh | head -n 1 | cut -f 3,3 -d " " | xargs -i kill {}
    ps -o "%p %a" | grep crane2.sh | head -n 1 | cut -f 3,3 -d " " | xargs -i kill {}
    echo "Finishing my work"
    exit 0
}

trap kill_cranes SIGINT
trap start_cranes USR1
# start_cranes()

while true
do
sleep 1
done