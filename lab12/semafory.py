import threading
import time

sem = threading.Semaphore(1)

def f1():
    while True:
        with sem:
            print(1)
        time.sleep(5)

def f2():
    while True:
        with sem:
            print(2)
        time.sleep(5)

t1 = threading.Thread(target=f1)
t1.start()
t2 = threading.Thread(target=f2)
t2.start()
		