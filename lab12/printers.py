import threading
import time
import random

printer = threading.Semaphore()


def printuj():
    while True:
        printer.acquire()
        period = random.randint(1,10)
        print(f"Printing for {period}")
        time.sleep(period)
        printer.release()


while True:
    chance = random.randint(1,100)
    if (chance>74):
        worker = threading.Thread(target=printuj)
        print("worker trying to print")
        worker.start()
    time.sleep(.5)