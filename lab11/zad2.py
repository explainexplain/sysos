import os
import signal # pobranie modułu signal
import time
import sys

def handle_sigint(sig_num, x):
    print(f"Przechwycono sygnał {os.getpid()}")

def handle_sighup(sig_num, x):
    # signal.signal(signal.SIGINT, signal.SIG_IGN)
    # signal.signal(signal.SIGINT, lambda a, x: sys.exit(0))
    signal.signal(signal.SIGINT, signal.SIG_DFL)

signal.signal(signal.SIGINT, handle_sigint)
signal.signal(signal.SIGHUP, handle_sighup)
while True:
    time.sleep(1)