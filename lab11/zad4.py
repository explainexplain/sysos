import os
import random
import signal
import time

check1 = True
check2 = True

fork1 = os.fork()
if fork1 > 0:
    fork2 = os.fork()
    if fork2 == 0:
        print(f"Child2: {os.getpid()}, daddy: {os.getppid()}")
        while True:
            time.sleep(1)

else:
    print(f"Child1: {os.getpid()}, daddy: {os.getppid()}")
    while True:
        time.sleep(1)

time.sleep(1)
while True:
    randomNumber=random.randint(1,50)
    if randomNumber < 10 and check1:
        os.kill(fork1, signal.SIGKILL)
        print("killed the firstborn")
        check1 = False
    if randomNumber > 40 and check2:
        check2 = False
        print("killed the last child")
        os.kill(fork2, signal.SIGKILL)
    if not check1 and not check2:
        break
    time.sleep(1)