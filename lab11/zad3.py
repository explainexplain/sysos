import os
import time
import random

forked = os.fork()
# print(forked)
if forked == 0:
    print(os.getpid(), os.getppid())
    while True:
        randomNumber = random.randint(1, 50)
        print(randomNumber)
        if randomNumber%5 == 0:
            print(f"your number {randomNumber}")
            # os._exit(0)
            break
        time.sleep(1)            
if forked > 0:
    os.wait()
    print(os.getpid())

print("And this one is printed by both of us")